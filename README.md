Dassert provides rich asserts for dictionaries.

# Why bother? #

Basic `unittest.assertDictEqual` just compares dictionaries using
`actual != expected` and `unittest.assertDictContainsSubset` doesn't
compare the dictionaries recursively. Basically, it would be great to
provide some sort of templating for dictionaries.

# Show me some examples! #

To get the magic in your `unittest` TestCase, use
`dassert.DassertMixin`.

```
#!python

import unittest, dassert, re


class TestCoolFeature(dassert.DassertMixin, unittest.TestCase):
    def test_dict_comparison(self):
        self.assertDictMatches({
            'ice-cream': {
                'flavor': 'chocolate',
                'price': lambda p: isinstance(p, int) and p > 0,
                'name': re.compile(r'^choco')
            }
        }, {
            'ice-cream': {
                'flavor': 'chocolate',
                'price': 4,
                'name': 'choco-who-cares',
                'another_ice_cream_attribute': 'whatever'
            },
            'another_key': 'not_important'
        })
```
