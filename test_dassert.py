import copy
import dassert
import re
import unittest


class TestBasicDictionaryMatching(dassert.DassertMixin, unittest.TestCase):
    """Tests that plain dictionaries match correctly.

    This test case does not cover regexes or methods in the expected
    dictionary.
    """
    def test_that_empty_dicts_match(self):
        self.assertDictMatches(expected={}, actual={})

    def test_that_single_level_identical_dicts_match(self):
        test_dicts = (
            {'a': 1, 'b': '2'},
            {'a': []},
            {'a': (2, 3)},
        )
        for test_dict in test_dicts:
            self.assertDictMatches(test_dict, copy.deepcopy(test_dict))

    def test_that_single_level_dissimilar_dicts_dont_match(self):
        test_dicts = (
            ({'a': 1, 'b': '2'},
             {'a': '2', 'b': 1}),

            ({'a': 1, 'b': 2},
             {'a': 1})
        )
        for expected, actual in test_dicts:
            with self.assertRaises(AssertionError):
                self.assertDictMatches(expected, actual)

    def test_that_multi_level_identical_dicts_match(self):
        test_dicts = (
            {'a': {'b': 1}},
            {'a': {'b': {'c': []}}},
            {'a': {'b': {'c': []}, 'd': 3}},
        )
        for test_dict in test_dicts:
            self.assertDictMatches(test_dict, copy.deepcopy(test_dict))

    def test_that_multi_level_dissimilar_dicts_dont_match(self):
        test_dicts = (
            ({'a': {'b': 1}},
             {'a': {'b': 3}}),

            ({'a': {'b': 1}},
             {'a': {'c': 1}}),

            ({'a': {'b': {'c': []}, 'd': 3}},
             {'a': {'b': {'c': [1]}, 'd': 3}}),

            ({'a': {'b': 1}},
             {'a': {}}),
        )
        for expected, actual in test_dicts:
            with self.assertRaises(AssertionError):
                self.assertDictMatches(expected, actual)

    def test_that_expected_can_be_subset_of_actual(self):
        self.assertDictMatches({}, {'a': 1})
        self.assertDictMatches({'a': 1}, {'a': 1, 'b': 2})
        self.assertDictMatches({'a': {'b': 1}}, {'a': {'b': 1, 'c': {3, 4}}})

    def test_that_expected_cannot_be_superset_of_actual(self):
        with self.assertRaises(AssertionError):
            self.assertDictMatches({'a': 1}, {})

    def test_that_sequence_in_dict_matches(self):
        test_dicts = (
            {'a': [1, 2, 3]},
            {'a': [1, [2, 3, 4]]},
            {'a': [1, {2, 3, 4, 'a'}]},
        )
        for test_dict in test_dicts:
            self.assertDictMatches(test_dict, copy.deepcopy(test_dict))


class TestRegexMatching(dassert.DassertMixin, unittest.TestCase):
    def test_that_string_can_be_matched_by_pattern(self):
        self.assertDictMatches({'a': re.compile(r'^foo')},
                               {'a': 'foobar'})
        self.assertDictMatches({'a': {'b': re.compile(r'^foo')}},
                               {'a': {'b': 'foobar'}})


class TestFunctionMatching(dassert.DassertMixin, unittest.TestCase):
    def test_that_value_can_be_matched_by_function(self):
        """A function can either return a Falsy value (except `None`) or
        raise an `AssertionError`.

        This allows a function to return the result of the evaluation of a
        conditional or to defer to another assertion.
        """
        self.assertDictMatches({'a': lambda x: True}, {'a': 'whocares'})
        self.assertDictMatches({'a': lambda x: x.startswith('who')},
                               {'a': 'whocares'})
        self.assertDictMatches({'a': lambda x: self.assertEquals('b', x)},
                               {'a': 'b'})
        with self.assertRaises(AssertionError):
            self.assertDictMatches({'a': lambda x: self.assertEquals('b', x)},
                                   {'a': 'c'})
            self.assertDictMatches({'a': lambda x: x.startswith('b')},
                                   {'a': 'c'})
