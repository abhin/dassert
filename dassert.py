import collections
import itertools
import re

from unittest.util import safe_repr


class DassertMixin(object):
    """Adds assertions to ``unittest.TestCase``"""
    def _compare_mappings(self, expected, actual):
        missing = []
        mismatched = []
        for key, value in expected.iteritems():
            if key not in actual:
                missing.append(key)
            elif isinstance(value, re._pattern_type):
                if not value.match(actual[key]):
                    mismatched.append('%s: %s did not match pattern %s' %
                                      (safe_repr(key),
                                       safe_repr(actual[key]),
                                       safe_repr(value.pattern)))
            elif callable(value):
                try:
                    val = value(actual[key])
                    if val or val is None:
                        continue
                except AssertionError:
                    pass
                mismatched.append('%s: Callable %s did not evaluate to'
                                  ' True with argument %s' %
                                  (safe_repr(key),
                                   safe_repr(value),
                                   safe_repr(actual[key])))
            elif isinstance(value, collections.Mapping):
                inner_missing, inner_mismatched = self._compare_mappings(
                    value, actual[key]
                )
                missing.extend(
                    '{outer}.{inner}'.format(outer=key, inner=inner)
                    for inner in inner_missing
                )
                mismatched.extend(
                    '{outer}.{inner}'.format(outer=key, inner=inner)
                    for inner in inner_mismatched
                )
            elif isinstance(value, collections.Iterable):
                try:
                    if isinstance(value, list):
                        self.assertListEqual(value, actual[key])
                    elif isinstance(value, set):
                        self.assertSetEqual(value, actual[key])
                    elif isinstance(value, tuple):
                        self.assertTupleEqual(value, actual[key])
                    elif isinstance(value, basestring):
                        self.assertEqual(value, actual[key])
                    else:
                        raise ValueError('Unknown iterable type')
                except AssertionError:
                    mismatched.append('%s: expected: %s, actual: %s' %
                                      (safe_repr(key),
                                       safe_repr(value),
                                       safe_repr(actual[key])))
            elif value != actual[key]:
                mismatched.append('%s: expected: %s, actual: %s' %
                                  (safe_repr(key),
                                   safe_repr(value),
                                   safe_repr(actual[key])))

        return missing, mismatched

    def assertDictMatches(self, expected, actual, msg=None):
        missing, mismatched = self._compare_mappings(expected, actual)

        if not (missing or mismatched):
            return

        standard_msg = ''
        if missing:
            standard_msg = 'Missing: %s' % ','.join(
                safe_repr(m) for m in missing
            )
        if mismatched:
            if standard_msg:
                standard_msg += '; '
            standard_msg += 'Mismatched values: %s' % ','.join(mismatched)

        self.fail(self._formatMessage(msg, standard_msg))
