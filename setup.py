#!/usr/bin/env python

import os
from distutils.core import setup


def long_description():
    return open(os.path.join(os.path.dirname(__file__), 'README.md')).read()


setup(
    name='dassert',
    version='0.1.0',
    description='Rich assertions for Python datastructures',
    author='Abhin Chhabra',
    long_description=long_description(),
    url='https://bitbucket.org/abhin/dassert',
    py_modules=['dassert'],
)
